import { createSlice } from "@reduxjs/toolkit";

createSlice({
    name:'cart',
    initialState:{
        items: [],
        totalQuantity: 0
    },
    reducers: {
        addItemToCart(){},
        removeItemFromCart(){}
    }
})