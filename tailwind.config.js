/** @type {import('tailwindcss').Config} */

module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
    "./node_modules/@engineering/rcl-custom-project/src/**/*.{js,jsx,ts,tsx}"
  ],
  theme: {
      extend: {
          colors:{
              pri:{
                  main:'#F25D23',
                  sec:'#FEE8D6'
              },
              alt:{
                  main:'#3158F6',
                  light:"#1D2CD8",
              }
          }
      },
  },
  plugins: [
require('@tailwindcss/forms'),
  ],
}

